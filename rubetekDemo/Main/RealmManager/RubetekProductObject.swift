//
//  RubetekProductObject.swift
//  rubetekDemo
//
//  Created by Vlad on 12.08.2021.
//

import Foundation

class RubetekProductObject : RubetekObject {
    
    @objc dynamic var name : String? = nil
    @objc dynamic var room : String? = nil
    @objc dynamic var snapshot : Data? = nil
    @objc dynamic var favorites : Bool = false
    
    func customInit(id : Int, name : String?, room : String?, snapshot : String?, favorites : Bool?, completion : @escaping () -> ()) {
        
        self.id = id
        self.name = name ?? RubetekProductObject.services.constants.defaultName
        self.room = room ?? RubetekProductObject.services.constants.defaultRoom
        self.favorites = favorites ?? false
        
        if snapshot != nil {
            RubetekProductObject.services.networkManager.get(.image(stringURL: snapshot!), dataType: Data.self) { (result) in
                switch result {
                case .success(let data):
                    self.snapshot = data
                    completion()
                case .failure(let error):
                    RubetekProductObject.services.errorHandler.handle(error)
                    completion()
                }
            }
        } else { completion() }
    }
    
    func toggleFavorite(completion : @escaping () -> ()) {
        self.edit {
            self.favorites.toggle()
            completion()
        }
    }
}
