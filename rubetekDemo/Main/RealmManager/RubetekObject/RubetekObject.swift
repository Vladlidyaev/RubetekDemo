//
//  RubetekObject.swift
//  rubetekDemo
//
//  Created by Vlad on 09.08.2021.
//

import Foundation
import RealmSwift

class RubetekObject : Object {
    
    static let services = RubetekServices.shared
    @objc dynamic var id = 0
    
    func edit(completion : @escaping () -> ()) {
        DispatchQueue.main.async {
            let realmDataBase = try! Realm()
            do {
                try realmDataBase.write {
                    completion()
                }
            } catch {
                RubetekObject.services.errorHandler.handle(error)
            }
        }
    }
    
    static func get(id : Int, completion : @escaping (Self?) -> ()) {
        DispatchQueue.main.async {
            let realmDataBase = try! Realm()
            let objectByID = realmDataBase.objects(self).filter{ $0.id == id }.first
            guard let object = objectByID else {
                completion(nil)
                return
            }
            completion(object as? Self)
        }
    }
    
    static func get(completion : @escaping ([Self]?) -> ()) {
        DispatchQueue.main.async {
            let realmDataBase = try! Realm()
            let objects = Array(realmDataBase.objects(self))
            completion(objects as? [Self])
        }
    }
    
    
    static func delete(completion : @escaping (Error?) -> ()) {
        DispatchQueue.main.async {
            let realmDataBase = try! Realm()
            let objects = realmDataBase.objects(self)
            do {
                try realmDataBase.safeWrite({
                    realmDataBase.delete(objects)
                })
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
    
    static func save(objects : [RubetekObject],completion : @escaping (Error?) -> ()) {
        DispatchQueue.main.async {
            do {
                let realmDataBase = try! Realm()
                try realmDataBase.safeWrite({
                    realmDataBase.add(objects)
                })
                completion(nil)
            } catch {
                completion(error)
            }
        }
    }
}

extension Realm {
    func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
