//
//  Configurates.swift
//  rubetekDemo
//
//  Created by Vlad on 09.08.2021.
//

import Foundation
import UIKit

enum menuPages : String, CaseIterable {
    case cameras = "Камеры"
    case doors = "Двери"
}

class ConstantParameters {
    
    static var shared: ConstantParameters = ConstantParameters()
    private init() {}
    
    let backgroundQueue = DispatchQueue(label: "rubetek.background", qos: .background, attributes: .concurrent)
    
    let defaultName = "No name"
    let defaultRoom = "No room"
    let defaultImage = UIImage(systemName: "questionmark.circle")!
    
    let rubetekCameraCellID = "RubetekCameraCell"
    let rubetekDoorCellID = "RubetekDoorCell"
    let rubetekTableViewHeaderID = "RubetekTableViewHeader"
    
    let likeActionImage = UIImage(systemName: "star")!
    let likeActionTitle : String? = nil
    let editActionImage = UIImage(systemName: "square.and.pencil")!
    let editActionTitle : String? = nil
    let editActionAlert = (
        title : "Edit name.",
        subtitle : "Please enter the new name.",
        actionTitle : "Edit",
        cancelTitle : "Cancel",
        inputPlaceholder: "Door name"
    )
    
    
    //  UI
    
    let backgroundColor : UIColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    let cleanWhiteColor : UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let DetailsColor : UIColor = #colorLiteral(red: 0.01176470588, green: 0.662745098, blue: 0.9568627451, alpha: 1)
    let TextColor : UIColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    
    let cellCornerRadius : CGFloat = 10
    
    let favoriteIcon = UIImage(systemName: "star.fill")!
    let lockIcon = UIImage(systemName: "lock.fill")!
    let protectedIcon = UIImage(systemName: "shield.fill")!
}

extension ConstantParameters: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
