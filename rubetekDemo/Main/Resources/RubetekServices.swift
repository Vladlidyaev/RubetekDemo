//
//  RubetekServices.swift
//  rubetekDemo
//
//  Created by Vlad on 12.08.2021.
//

import Foundation

class RubetekServices {
    
    static var shared: RubetekServices = RubetekServices()
    private init() {}
    
    let networkManager = RubetekNetworkManager.shared
    let constants = ConstantParameters.shared
    let errorHandler = RubetekErrorManager.shared
    
}

extension RubetekServices: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
