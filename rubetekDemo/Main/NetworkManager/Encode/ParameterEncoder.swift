//
//  ParameterEncoder.swift
//  rubetekDemo
//
//  Created by Vlad on 07.08.2021.
//

import Foundation

protocol ParameterEncoder {
    
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
    
}
