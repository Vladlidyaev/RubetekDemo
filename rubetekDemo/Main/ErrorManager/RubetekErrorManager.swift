//
//  RubetekErrorManager.swift
//  rubetekDemo
//
//  Created by Vlad on 12.08.2021.
//

import Foundation

class RubetekErrorManager {
    
    static var shared: RubetekErrorManager = RubetekErrorManager()
    private init() {}
    
    func handle(_ error : Error ) {
        print("============CAUGHT ERROR============")
        print(error.localizedDescription)
        print("------------------------------------")
    }
}

extension RubetekErrorManager: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
}
