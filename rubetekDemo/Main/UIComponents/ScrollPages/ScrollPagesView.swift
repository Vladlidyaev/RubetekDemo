//
//  ScrollPagesView.swift
//  rubetekDemo
//
//  Created by Vlad on 10.08.2021.
//

import Foundation
import UIKit

@IBDesignable
class ScrollPagesView : UIScrollView, UIScrollViewDelegate {
    
    private weak var bindingControl : ScrollPagesControl? = nil
    
    private var pageList : [String] = []
    private var pagesViews : [UIView] = []
    
    private var animationTime : TimeInterval = 0.2
    private var startPage : Int = 0
    private var currentPage : Int = 0
    
    private var isScrollViewFree : Bool = true
    
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard !pageList.isEmpty else { return }
        preparingUI()
    }
    

    
    private func preparingUI() {
        
        self.delegate = self
        self.isPagingEnabled = true
        self.showsHorizontalScrollIndicator = false
        
        let viewWidth = self.frame.width
        let viewHeight = self.frame.height
        
        self.contentSize = CGSize(width: viewWidth * CGFloat(pageList.count), height: viewHeight)
        
        pagesViews.enumerated().forEach { (index,view) in
            view.frame =  CGRect(x: CGFloat(index) * viewWidth, y: .zero, width: viewWidth, height: viewHeight)
            self.addSubview(view)
        }
        scrollToPage(self.startPage)
        updateCurrentPage(newCurrentPage: startPage)
    }
    
    func bindWith(_ control : ScrollPagesControl, pagesViews : [UIView]) {
        pageList = control.pageList
        self.pagesViews = pagesViews
        startPage = control.currentPage
        bindingControl = control
        animationTime = control.animationTime
        layoutSubviews()
    }
    
    func scrollToPage(_ index : Int ) {
        guard   (index != getCurrentPageIndex())
                    && (pageList.startIndex...pageList.endIndex).contains(index)
                    && (!pageList.isEmpty)
                    && isScrollViewFree else { return }
        isScrollViewFree = false
        
        DispatchQueue.main.async { [self] in
            UIView.animate(withDuration: animationTime) {
                self.contentOffset.x = CGFloat(CGFloat(index)*self.frame.width)
            } completion: { _ in
                updateCurrentPage(newCurrentPage: index)
                isScrollViewFree = true
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = getCurrentPageIndex()
        guard   !pageList.isEmpty
                    && index != currentPage
                    && bindingControl != nil
                    && isScrollViewFree else { return }
        bindingControl!.moveUnderlineToPage(index)
        updateCurrentPage(newCurrentPage: index)
    }
    
    private func getCurrentPageIndex() -> Int {
        return Int(round(self.contentOffset.x/self.frame.width))
    }

    //  OBSERVER
    
    private lazy var subscribers : [WeakPageSubscriber] = []

    func subscribeOnCurrentPage(_ subscriber: PageSubscriber) {
        subscribers.append(WeakPageSubscriber(value: subscriber))
    }

    func unsubscribeOnCurrentPage(_ subscriber: PageSubscriber) {
        subscribers.removeAll(where: { $0.value === subscriber })
    }

    private func notify() {
        subscribers.forEach { $0.value?.pagesViewUpdateCurrentPage(currentPage: currentPage)}
    }

    private func updateCurrentPage(newCurrentPage : Int) {
        startPage = newCurrentPage
        currentPage = newCurrentPage
        notify()
    }
}
