//
//  ScrollPagesControl.swift
//  rubetekDemo
//
//  Created by Vlad on 10.08.2021.
//

import Foundation
import UIKit

@IBDesignable
class ScrollPagesControl : UIControl {
    
    private var bindingView : ScrollPagesView? = nil
    private var underlineView : UIView? = nil
    
    private(set) var pageList : [String] = ["Title1","Title2"]
    private var sections : [UIButton] = []
    
    private(set) var currentPage : Int = 0
    private(set) var animationTime : TimeInterval = 0.2
    private var isUnderlineViewFree : Bool = true
    
    private var pageTitleFont: UIFont = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.light)
    
    
    
    @IBInspectable var selectorHeight: CGFloat = 3 {
        didSet { layoutSubviews() }
    }
    
    @IBInspectable var selectorColor: UIColor = .blue {
        didSet { layoutSubviews() }
    }
    
    @IBInspectable var textColor: UIColor = .black {
        didSet { layoutSubviews() }
    }
    
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard !pageList.isEmpty else { return }
        initSections()
        preparingUnderlineView()
        preparingStackView()
    }
    
    
    
    func bindWith(_ pages : [String], view : ScrollPagesView) {
        pageList = pages
        bindingView = view
        layoutSubviews()
    }
    
    func moveUnderlineToPage(_ index : Int) {
        goToPage(index, scrollingPages: false)
    }
    
    private func preparingStackView() {
        
        let stack = UIStackView(arrangedSubviews: sections)
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        stack.frame = CGRect(x: .zero, y: .zero, width: self.frame.width, height: self.frame.height)
        self.addSubview(stack)
    }
    
    private func preparingUnderlineView() {
        
        let backgroundSectionLayer = CAGradientLayer()
        backgroundSectionLayer.frame = CGRect(x: .zero, y: self.frame.height - selectorHeight, width: self.frame.width, height: selectorHeight)
        backgroundSectionLayer.colors = [UIColor.gray.withAlphaComponent(0.1).cgColor, UIColor.clear.cgColor]
        self.layer.addSublayer(backgroundSectionLayer)
        
        
        let sectionWidth = self.frame.width / CGFloat(pageList.count)
        underlineView = UIView(frame: CGRect(x: CGFloat(currentPage)*sectionWidth, y: self.frame.height - selectorHeight, width: sectionWidth, height: selectorHeight))
        underlineView!.layer.cornerRadius = selectorHeight/2
        underlineView!.backgroundColor = selectorColor
        self.addSubview(self.underlineView!)
    }
    
    private func initSections() {
        
        sections = [UIButton]()
        sections.removeAll()
        self.subviews.forEach { $0.removeFromSuperview() }
        pageList.forEach { (pageName) in
            let btn = self.preparingSection(title: pageName)
            sections.append(btn)
        }
    }
    
    private func preparingSection(title : String) -> UIButton {
        let button = UIButton()
        button.addTarget(self, action: #selector(sectionTapped), for: .touchUpInside)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = pageTitleFont
        button.setTitleColor(textColor, for: .normal)
        button.backgroundColor = .clear
        
        return button
    }
    
    private func goToPage(_ index : Int, scrollingPages : Bool) {
        
        guard   (index != currentPage) &&
                    (pageList.startIndex...self.pageList.endIndex).contains(index) &&
                    (underlineView != nil) &&
                    (!pageList.isEmpty) &&
                    (isUnderlineViewFree)   else { return }
        
        isUnderlineViewFree = false
        
        let undelinePosition = self.frame.width / CGFloat(pageList.count) * CGFloat(index)
        
        if scrollingPages && bindingView != nil {
            bindingView!.scrollToPage(index)
        }
        
        UIView.animate(withDuration: animationTime) { [self] in
            underlineView!.frame.origin.x = undelinePosition
        } completion: { [self] _ in
            isUnderlineViewFree = true
            currentPage = index
        }
    }
    
    @objc private func sectionTapped(_ sender: UIButton) {
        for (sectionIndex, section) in self.sections.enumerated() {
            if sender == section {
                self.goToPage(sectionIndex, scrollingPages: true)
            }
        }
    }
}
