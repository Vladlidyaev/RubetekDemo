//
//  RubetekCell.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import UIKit

class RubetekCell: UITableViewCell {
    
    let services = RubetekServices.shared
    var objectData : RubetekProductObject = RubetekProductObject()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func customInit(_ data : Any) {}
    
    func preparingBackground(backgroundView : UIView, imageView : UIImageView?) {
        
        backgroundView.layer.cornerRadius = services.constants.cellCornerRadius
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOpacity = 0.1
        backgroundView.layer.shadowOffset = .zero
        backgroundView.layer.shadowRadius = services.constants.cellCornerRadius/6
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 6)
        
        guard let imageView = imageView else { return }
        imageView.layer.cornerRadius = services.constants.cellCornerRadius
        imageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func setupFavoriteIcon(_ value : Bool, imageView : UIImageView) {
        imageView.image = services.constants.favoriteIcon
        if value {
            imageView.alpha = 1
        } else {
            imageView.alpha = 0.2
        }
    }
    
    func setupLockIcon(_ lockIcon : UIImageView) {
        lockIcon.image = services.constants.lockIcon
        lockIcon.alpha = 0.2
    }
}
