//
//  RubetekTableView.swift
//  rubetekDemo
//
//  Created by Vlad on 10.08.2021.
//

import Foundation
import UIKit

class RubetekTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    var dataSection : [RubetekSectionModel] = []
    
    struct RubetekSectionModel {
        var title : String
        var headerId : String
        var items : [RubetekSectionItem]
    }
    
    struct RubetekSectionItem {
        var cellId : String
        var value : Any
    }
    
    
    
    init(frame: CGRect, pageIndex : Int) {
        super.init(frame: frame, style: UITableView.Style.plain)
        self.tableViewConfig()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.tableViewConfig()
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.tableViewConfig()
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.dataSection.count > section else { return 0 }
        return self.dataSection[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: dataSection[indexPath.section].items[indexPath.row].cellId, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: dataSection[section].headerId) as! RubetekTableViewHeader
        header.titleLabel.text = self.dataSection[section].title
        return header
    }
    

    
    private func tableViewConfig() {
        self.delegate = self
        self.dataSource = self
        self.separatorStyle = .none
        
        self.tableFooterView = UIView()
        self.addTableViewConfigs()
        self.addRefreshController()
    }
    
    func addTableViewConfigs() {}
    
    private func addRefreshController() {
        let refreshControl : UIRefreshControl = {
            let rc = UIRefreshControl()
            rc.tintColor = #colorLiteral(red: 0.01176470588, green: 0.662745098, blue: 0.9568627451, alpha: 1)
            rc.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            return rc
        }()
        self.refreshControl = refreshControl
    }
    
    @objc func refreshData(_ sender : UIRefreshControl) {}
}
