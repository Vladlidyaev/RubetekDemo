//
//  RubetekProductTableView.swift
//  rubetekDemo
//
//  Created by Vlad on 13.08.2021.
//

import UIKit

class RubetekProductTableView: RubetekTableView {
    
    let services = RubetekServices.shared
    
    override func addTableViewConfigs() {
        self.register(UINib(nibName: services.constants.rubetekCameraCellID, bundle: nil), forCellReuseIdentifier: services.constants.rubetekCameraCellID)
        self.register(UINib(nibName: services.constants.rubetekDoorCellID, bundle: nil), forCellReuseIdentifier: services.constants.rubetekDoorCellID)
        self.register(UINib(nibName: services.constants.rubetekTableViewHeaderID, bundle: nil), forHeaderFooterViewReuseIdentifier: services.constants.rubetekTableViewHeaderID)
        
        self.backgroundColor = services.constants.backgroundColor
    }
    
    func setData(_ data : [RubetekProductObject]) {
        self.dataSection = self.getSection(data: data)
        self.reloadData()
    }
    
    func getSection(data : [RubetekProductObject]) -> [RubetekSectionModel] {
        var dictionary : [ String : [RubetekProductObject]] = [:]
        data.forEach { (object) in
            let room = object.room!
            if (dictionary[room] == nil) {
                dictionary[room] = [object]
            } else {
                dictionary[room]!.append(object)
            }
        }
        return dictionary
            .map { RubetekSectionModel(title: $0.key,
                                       headerId: services.constants.rubetekTableViewHeaderID,
                                       items: $0.value.map { value in
                                        if value.snapshot == nil {
                                            return RubetekSectionItem(cellId: services.constants.rubetekDoorCellID, value: value)
                                        } else {
                                            return RubetekSectionItem(cellId: services.constants.rubetekCameraCellID, value: value)
                                        }
                                       })
            } .sorted { $0.title < $1.title }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = dataSection[indexPath.section].items[indexPath.row].value
        
        (self.parentViewController as? HomeViewController)?.pushDetailsViewController(with: object as! RubetekProductObject)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let data = self.dataSection[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: data.cellId, for: indexPath) as! RubetekCell
        cell.customInit(data.value)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favorite = self.favoriteCell(indexPath: indexPath)
        let actions = UISwipeActionsConfiguration(actions: [favorite])
        return actions
    }
    
    func favoriteCell(indexPath : IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: services.constants.likeActionTitle) { (_, _, _) in
            
            let cell = self.cellForRow(at: indexPath) as? RubetekCell
            guard let selectedCell = cell else { fatalError() }
            
            let object = selectedCell.objectData
            
            object.toggleFavorite {
                self.reloadData()
            }
        }
        action.backgroundColor = services.constants.backgroundColor
        action.image = services.constants.likeActionImage.tint(with: services.constants.DetailsColor)
        return action
    }
    
    func updateData() {}
}

extension UIImage {
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()

        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
