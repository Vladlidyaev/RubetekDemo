//
//  RubetekViewController.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import Foundation
import UIKit

protocol StorageObjectViewController : UIViewController {
    var object : RubetekProductObject { get set }
}

class RubetekViewController : UIViewController {
    
    let services = RubetekServices.shared
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func pushViewController<T : StorageObjectViewController>(vcClass : T.Type, storyboardID : String, object : RubetekProductObject) {
        
        if let viewController = (self.storyboard?.instantiateViewController(withIdentifier: storyboardID) as? T) {
            viewController.object = object
            self.navigationController?.pushViewController(viewController as UIViewController, animated: true)
        }
    }
}
