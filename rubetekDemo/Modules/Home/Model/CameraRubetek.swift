//
//  CameraRubetek.swift
//  rubetekDemo
//
//  Created by Vlad on 09.08.2021.
//

import Foundation

class CameraRubetek: RubetekProductObject {
    
    @objc dynamic var rec : Bool = false
    
    func customInit(id: Int, name: String?, room: String?, snapshot: String?, favorites: Bool?, rec: Bool? , completion: @escaping () -> ()) {
        self.customInit(id: id, name: name, room: room, snapshot: snapshot, favorites: favorites) {
            
            self.rec = rec ?? false
            completion()
        }
    }
    
    static func getObjects(completion : @escaping ([CameraRubetek]) -> ()) {
        services.constants.backgroundQueue.async {
            _ = CameraRubetek.get { (result) in
                
                guard let objects = (result as? [CameraRubetek]) else {
                    completion([])
                    return
                }
                
                guard objects.isEmpty else {
                    completion(objects)
                    return
                }
                
                CameraRubetek.refreshData { (updateResult) in
                    completion(updateResult)
                }
            }
        }
    }
    
    static func refreshData(completion : @escaping ([CameraRubetek]) -> ()) {
        services.constants.backgroundQueue.async {
            
            services.networkManager.get(.cameras, dataType: camerasListCodable.self) { (result) in
                
                switch result {
                
                case .success(let data):
                    
                    guard let array = data.data?.cameras else {
                        completion([])
                        return
                    }
                    
                    var cameraRubetekArray = [CameraRubetek]()
                    let group = DispatchGroup()
                    array.forEach { (cameraCodable) in
                        
                        group.enter()
                        let element = CameraRubetek()
                        element.customInit(id: cameraCodable.id, name: cameraCodable.name, room: cameraCodable.room, snapshot: cameraCodable.snapshot, favorites: cameraCodable.favorites, rec: cameraCodable.rec) {
                            
                            cameraRubetekArray.append(element)
                            group.leave()
                        }
                    }
                    
                    group.notify(queue: services.constants.backgroundQueue) {
                        
                        CameraRubetek.delete { (error) in
                            
                            guard error == nil else {
                                services.errorHandler.handle(error!)
                                completion([])
                                return
                            }
                            
                            CameraRubetek.save(objects: cameraRubetekArray) { (error) in
                                guard error == nil else {
                                    services.errorHandler.handle(error!)
                                    completion([])
                                    return
                                }
                                completion(cameraRubetekArray)
                            }
                        }
                    }
                    
                case .failure(let error):
                    services.errorHandler.handle(error)
                    completion([])
                }
            }
        }
    }
}
