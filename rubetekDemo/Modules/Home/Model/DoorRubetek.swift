//
//  DoorRubetek.swift
//  rubetekDemo
//
//  Created by Vlad on 09.08.2021.
//

import Foundation

class DoorRubetek: RubetekProductObject {
    
    func setNewName(_ name : String, completion : @escaping () -> ()) {
        self.edit {
            self.name = name
            completion()
        }
    }
    
    static func getObjects(completion : @escaping ([DoorRubetek]) -> ()) {
        services.constants.backgroundQueue.async {
            _ = DoorRubetek.get { (result) in
                
                guard let objects = (result as? [DoorRubetek]) else {
                    completion([])
                    return
                }
                
                guard objects.isEmpty else {
                    completion(objects)
                    return
                }
                
                DoorRubetek.refreshData { (updateResult) in
                    completion(updateResult)
                }
            }
        }
    }
    
    static func refreshData(completion : @escaping ([DoorRubetek]) -> ()) {
        services.constants.backgroundQueue.async {
            
            services.networkManager.get(.doors, dataType: doorListCodable.self) { (result) in
                
                switch result {
                
                case .success(let data):
                    
                    guard let array = data.data else {
                        completion([])
                        return
                    }
                    
                    var doorRubetekArray = [DoorRubetek]()
                    let group = DispatchGroup()
                    array.forEach { (doorCodable) in
                        
                        group.enter()
                        let element = DoorRubetek()
                        element.customInit(id: doorCodable.id, name: doorCodable.name, room: doorCodable.room, snapshot: doorCodable.snapshot, favorites: doorCodable.favorites) {
                            
                            doorRubetekArray.append(element)
                            group.leave()
                        }
                    }
                    
                    group.notify(queue: services.constants.backgroundQueue) {
                        
                        DoorRubetek.delete { (error) in
                            
                            guard error == nil else {
                                services.errorHandler.handle(error!)
                                completion([])
                                return
                            }
                            
                            DoorRubetek.save(objects: doorRubetekArray) { (error) in
                                guard error == nil else {
                                    services.errorHandler.handle(error!)
                                    completion([])
                                    return
                                }
                                completion(doorRubetekArray)
                            }
                        }
                    }
                    
                case .failure(let error):
                    services.errorHandler.handle(error)
                    completion([])
                }
            }
        }
    }
}
