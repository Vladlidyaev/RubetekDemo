//
//  RubetekCameraTableView.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import UIKit

class RubetekCameraTableView: RubetekProductTableView {
    
    override func updateData() {
        CameraRubetek.getObjects(completion: { (data) in
            self.setData(data)
        })
    }
    
    @objc override func refreshData(_ sender: UIRefreshControl) {
        CameraRubetek.refreshData { (data) in
            DispatchQueue.main.async {
                sender.endRefreshing()
            }
            self.setData(data)
        }
    }
}
