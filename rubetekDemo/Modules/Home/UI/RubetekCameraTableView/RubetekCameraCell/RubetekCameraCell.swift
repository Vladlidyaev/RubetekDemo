//
//  RubetekCameraCell.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import UIKit

@IBDesignable
class RubetekCameraCell: RubetekCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var protectedIcon : UIImageView!
    @IBOutlet weak var favoriteIcon : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        preparingBackground(backgroundView: background, imageView: mainImageView)
    }
    
    override func customInit(_ data : Any) {
        
        if let data = data as? CameraRubetek {
            
            self.objectData = data
            self.titleLabel.text = data.name
            setupMainImage(data.snapshot)
            self.setupFavoriteIcon(data.favorites, imageView: favoriteIcon)
            setupProtectedIcon()
            return
            
        } else {
            
            guard let data = data as? DoorRubetek else { return }
            self.objectData = data
            self.titleLabel.text = data.name
            setupMainImage(data.snapshot)
            self.setupFavoriteIcon(data.favorites, imageView: favoriteIcon)
            self.setupLockIcon(protectedIcon)
            return
        }
    }
    
    func setupMainImage(_ data : Data?) {
        if data != nil {
            var image = UIImage(data: data!)
            if image == nil {
                image = services.constants.defaultImage
            }
            self.mainImageView.image = image
        }
    }
    
    func setupProtectedIcon() {
        protectedIcon.image = services.constants.protectedIcon
        if (objectData as! CameraRubetek).rec {
            protectedIcon.alpha = 1
        } else {
            protectedIcon.alpha = 0.2
        }
    }
}
