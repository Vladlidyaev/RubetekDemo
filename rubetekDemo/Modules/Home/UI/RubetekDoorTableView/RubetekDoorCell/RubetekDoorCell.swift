//
//  RubetekDoorCell.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import UIKit

@IBDesignable
class RubetekDoorCell: RubetekCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var lockIcon: UIImageView!
    @IBOutlet weak var favoriteIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        preparingBackground(backgroundView: background, imageView: nil)
    }
    
    override func customInit(_ data : Any) {
        
        guard let data = data as? DoorRubetek else { return }
        self.objectData = data
        self.titleLabel.text = data.name
        self.setupFavoriteIcon(data.favorites, imageView: favoriteIcon)
        self.setupLockIcon(lockIcon)
    }
}
