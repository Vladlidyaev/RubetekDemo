//
//  RubetekDoorTableView.swift
//  rubetekDemo
//
//  Created by Vlad on 11.08.2021.
//

import UIKit

class RubetekDoorTableView: RubetekProductTableView {
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let favorite = self.favoriteCell(indexPath: indexPath)
        let edit = self.editCell(indexPath: indexPath)
        let actions = UISwipeActionsConfiguration(actions: [edit,favorite])
        return actions
    }
    
    private func editCell(indexPath : IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: services.constants.editActionTitle) { (_, _, _) in
            
            let selectedCell = self.cellForRow(at: indexPath) as? RubetekCell
            guard let cell = selectedCell else { fatalError() }
            
            let cellObject = cell.objectData as? DoorRubetek
            guard let object = cellObject else { fatalError() }
            
            let plate = self.services.constants.editActionAlert
            self.showInputDialog(title: plate.title, subtitle: plate.subtitle, actionTitle: plate.actionTitle, cancelTitle: plate.cancelTitle, inputPlaceholder: plate.inputPlaceholder, inputKeyboardType: .default) { (_) in
                return
            } actionHandler: { (newName) in
                
                guard let name = newName else { fatalError() }
                guard !name.isEmpty else { fatalError() }
                
                object.setNewName(name) {
                    self.reloadData()
                }
            }
        }
        action.backgroundColor = services.constants.backgroundColor
        action.image = services.constants.editActionImage.tint(with: services.constants.DetailsColor)
        return action
    }
    
    override func updateData() {
        DoorRubetek.getObjects(completion: { (data) in
            self.setData(data)
        })
    }
    
    @objc override func refreshData(_ sender: UIRefreshControl) {
        DoorRubetek.refreshData { (data) in
            DispatchQueue.main.async {
                sender.endRefreshing()
            }
            self.setData(data)
        }
    }
}
