//
//  DetailsViewController.swift
//  rubetekDemo
//
//  Created by Vlad on 13.08.2021.
//

import UIKit

class DetailsViewController: RubetekViewController, StorageObjectViewController {
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!

    var object : RubetekProductObject = RubetekProductObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preparingUI()
    }
    
    func preparingUI() {
        if let title = object.name {
            titleLabel.text = title
        }
        
        if object.snapshot != nil {
            if let image = UIImage(data: object.snapshot!) {
                self.mainImage.image = image
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
