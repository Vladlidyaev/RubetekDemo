//
//  ViewController.swift
//  rubetekDemo
//
//  Created by Vlad on 07.08.2021.
//

import UIKit

class HomeViewController: RubetekViewController, PageSubscriber {
    
    @IBOutlet weak var control: ScrollPagesControl!
    @IBOutlet weak var pageView: ScrollPagesView!
    
    private var pagesViews : [(view : RubetekProductTableView, alredyShowed : Bool)] = []
    var tv : UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preparingUI()
        
        pageView.subscribeOnCurrentPage(self)
    }
    
    
    
    private func preparingUI() {
        
        pagesViews.append((view: RubetekCameraTableView(), alredyShowed: false))
        pagesViews.append((view: RubetekDoorTableView(), alredyShowed: false))
        
        control.bindWith(menuPages.allCases.map { $0.rawValue }, view: pageView)
        pageView.bindWith(control, pagesViews: pagesViews.map { $0.view })
        
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    func pagesViewUpdateCurrentPage(currentPage: Int) {
        
        if !pagesViews[currentPage].alredyShowed {
            pagesViews[currentPage].view.updateData()
            pagesViews[currentPage].alredyShowed = true
        }
    }
    
    func pushDetailsViewController(with object : RubetekProductObject) {
        self.pushViewController(vcClass: DetailsViewController.self, storyboardID: "DetailsViewController", object: object)
    }
    
    @objc func appMovedToBackground() {
        pagesViews.indices.forEach { pagesViews[$0].alredyShowed = false }
    }
}
